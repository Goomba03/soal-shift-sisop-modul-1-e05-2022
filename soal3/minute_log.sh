#!/bin/bash
tgl=$(date +"20%y%m%d")
wkt=$(date +"%r" | awk -F: '{print $1,$2,$3}' | tr -d "[:blank:]")
tlg_wkt="${tgl}${wkt}"

m_tot=$(free -m | awk 'FNR == 2 {print}' | awk '{print $2}')
m_used=$(free -m | awk 'FNR == 2 {print}' | awk '{print $3}')
m_free=$(free -m | awk 'FNR == 2 {print}' | awk '{print $4}')
m_shared=$(free -m | awk 'FNR == 2 {print}' | awk '{print $5}')
m_buff=$(free -m | awk 'FNR == 2 {print}' | awk '{print $6}')
m_avlb=$(free -m | awk 'FNR == 2 {print}' | awk '{print $7}')

s_tot=$(free -m | awk 'FNR == 3 {print}' | awk '{print $2}')
s_used=$(free -m | awk 'FNR == 3 {print}' | awk '{print $3}')
s_free=$(free -m | awk 'FNR == 3 {print}' | awk '{print $4}')

pth_sz=$(du -sh /home/$USER/ | awk '{print $1}')
pth_pth=$(du -sh /home/$USER/ | awk '{print $2}')

otp="${m_tot},${m_used},${m_free},${m_shared},${m_buff},${m_avlb},${s_tot},${s_used},${s_free},${pth_pth},${pth_sz}"

echo ${otp} > /home/$USER/log/metrics_${tlg_wkt}.log