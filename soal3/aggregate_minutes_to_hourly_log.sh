#!/bin/bash
tgl=$(date +"20%y%m%d")
wkt=$(date +"%r" | awk -F: '{print $1,$2,$3}' | tr -d "[:blank:]")
tlg_wkt="${tgl}${wkt}"

fl_all=$(ls /home/$USER/log/.)

#get time rn
hour_curr=$(date +"%H")
min_curr=$(date +"%M")
curr="${hour_curr}${min_curr}"
hour_bfr=$(echo $hour_curr | awk '{print $1 - 1}')
bfr="${hour_bfr}${min_curr}"

#Find the hour and minute of the log files that is between hour_bfr, min_curr and hour_curr, min curr
# grep semua yang diantara hour_bfr, min_curr and hour_curr, min curr index 17 sampai 20
# for i in $(seq $bfr $curr); do 
#     fil_nam="metrics_${tlg_wkt}.log$i"

# done

for f in $fl_all; do
    tmp=$(echo $f | cut -c17-20)

    #debug
    echo $bfr
    echo $tmp
    echo $f

    # if bfr <= tmp <= currW
    if [ $bfr -le $tmp -o $curr -ge $tmp ]
    then 
        # cat isi file lalu append ke file/variable sementara
        cat /home/$USER/log/$f | awk -F, '{print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11 $12}' >> /home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log
    fi
done

max1=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $1}' | sort -rn | head -n 1)
max2=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $2}' | sort -rn | head -n 1)
max3=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $3}' | sort -rn | head -n 1)
max4=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $4}' | sort -rn | head -n 1)
max5=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $5}' | sort -rn | head -n 1)
max6=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $6}' | sort -rn | head -n 1)
max7=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $7}' | sort -rn | head -n 1)
max8=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $8}' | sort -rn | head -n 1)
max9=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $9}' | sort -rn | head -n 1)

min1=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $1}' | sort -n | head -n 1)
min2=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $2}' | sort -n | head -n 1)
min3=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $3}' | sort -n | head -n 1)
min4=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $4}' | sort -n | head -n 1)
min5=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $5}' | sort -n | head -n 1)
min6=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $6}' | sort -n | head -n 1)
min7=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $7}' | sort -n | head -n 1)
min8=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $8}' | sort -n | head -n 1)
min9=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $9}' | sort -n | head -n 1)

avg1=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $1 } END { print total/NR }')
avg2=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $2 } END { print total/NR }')
avg3=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $3 } END { print total/NR }')
avg4=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $4 } END { print total/NR }')
avg5=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $5 } END { print total/NR }')
avg6=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $6 } END { print total/NR }')
avg7=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $7 } END { print total/NR }')
avg8=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $8 } END { print total/NR }')
avg9=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{ total += $9 } END { print total/NR }')

pth=$(/home/$USER/log/metrics_agg_tmp_${tlg_wkt}.log | awk '{print $10}'



if [[ "${line}" != [a-z] ]]; then
   echo INVALID
fi

# for f in "${arr[@]}"; do
# # get just the time, and specifically the hour and minute
# # if the hour and minute is from 1 hour before rn to time rn buat log lalu append semua data, jabarkan data lalu do the thing 
#    echo "$f"
# done


# awk 'BEGIN
# {
#   if ( max[$1] < $2 )
#      max[$1]=$2

#   tot[$1] +=$2
#   avg[$1]++
# }
# END {
#   for (i in max)
#     printf("[%s]: total->[%s] avg->[%.2f] max-> [%d]\n", i, tot[i], tot[i]/avg[i], max[i])
# }'

# sh /home/fathur45/Documents/VsCode/SISOP/Soal\ Shift\ Modul\ 1/aggregate_minutes_to_hourly_log.sh
# /home/$USER/log/metrics_20220226121620.log
# echo "15413,11332,853,566,3227,3189,2047,83,1964,/home/fathur45/,5,3G" | awk -F, '{print $1, $2, $3, $4, $5, $6, $7, $8, $9, $13}'

