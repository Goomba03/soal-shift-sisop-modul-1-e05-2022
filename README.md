# soal-shift-sisop-modul-1-E05-2022

praktikum sisop

## Soal 1
### main.sh
```
my_dir="$(dirname "$0")"
t_d=$(date +"%m/%d/%Y")
t_h=$(date +"%T")
t_d1=$(date +"%Y-%m-%d")
```
sebagai inisialisasi lokasi direktori beserta variabel yang menyimpan format tanggal dan waktu
<br /> <br /> 
```
echo enter your username
read -s name

if grep -q "user: $name" "$my_dir/users/user.txt";then
```
membaca input dan mencari jika input username ada di database user (user.txt)
<br /> <br />
```
echo enter your password
	read -s pass
	
	if grep -q "user: $name pass: $pass" "$my_dir/users/user.txt";then
```
membaca input dan mencari jika input username dan password sesuai dan ada di database user (user.txt)
<br /> <br />
```
echo LOGIN: INFO User $user logged in
		echo "${t_d} ${t_h} LOGIN: INFO User $user logged in" >> $my_dir/log.txt
		echo 'enter command ("dl N(number)" or "att")'
		read cmd
```
mengirimkan output login berhasil lalu menyimpannya ke log.txt
setelah itu mengirimkan output yang menanyakan pilihan user antara command dl N atau att dan membaca input setelahnya
<br /> <br />
```
	if [ "$cmd" == "att" ];then
			grep -c "LOGIN: " "$my_dir/log.txt"
```
jika user mengetik input att, maka program akan mengoutputkan jumlah percobaan login yang gagal dan berhasil
<br /> <br />
```
else 
			num=$(echo "$cmd" | sed 's/[^0-9]*//g')
			if [ -e $my_dir/"$t_d1"_$name.zip ];then
				unzip -P $pass $my_dir/"$t_d1"_$name.zip -d $my_dir/
				mkdir $my_dir/"$t_d1"_$name
				find $my_dir/$my_dir/"$t_d1"_$name -type f -print0 | xargs -0 mv -t $my_dir/"$t_d1"_$name
				last_file=$(ls -1 $my_dir/"$t_d1"_$name | tail -n 1)
				last_num=$(echo "$last_file" | sed 's/[^0-9]*//g')
				for((i=last_num+1; i<num+last_num+1; i++))
				do
				if [ $i -lt 10 ];then
					urut=0$i
				else
					urut=$i
				fi
				wget -nd -r -P $my_dir/"$t_d1"_$name / --output-document=$my_dir/"$t_d1"_$name/PIC_$urut.jpg https://loremflickr.com/320/240
				done
				zip --password $pass -r $my_dir/"$t_d1"_$name.zip $my_dir/"$t_d1"_$name
				base_dir=$(echo "$my_dir" | cut -d "/" -f2)
				rm -r $my_dir/$base_dir/
				rm -r $my_dir/"$t_d1"_$name/
			else
				if ! [ -d $my_dir/"$t_d1"_$name ];then
					mkdir $my_dir/"$t_d1"_$name
				fi
				for((i=1; i<num+1; i++))
				do
				if [ $i -lt 10 ];then
					urut=0$i
				else
					urut=$i
				fi
				wget -nd -r -P $my_dir/"$t_d1"_$name / --output-document=$my_dir/"$t_d1"_$name/PIC_$urut.jpg https://loremflickr.com/320/240
				done
				zip --password $pass -r $my_dir/"$t_d1"_$name.zip $my_dir/"$t_d1"_$name 
				rm -r $my_dir/"$t_d1"_$name/
			fi
		fi
```
jika user mengetikkan input dl, maka akan masuk ke else dan angka yang diinput tadi akan diekstrak ke variabel num. variabel num ini digunakan untuk melakukan loop sebanyak num yang dimana didalamnya terdapat **wget** untuk mendownload file gambar. terdapat dua kondisional yang dimana cara kerjanya mirip, kondisional pertama tereksekusi ketika file zip yang akan dibuat dari file yang didownload telah ada, sedangkan kondisional kedua tereksekusi ketika file zip yang akan dibuat dari file yang didownload belum ada

 maka kondisional pertama me-unzip lalu mengekstrak gambar dari folder yang telah diunzip, membuat folder baru yang disesuaikan dengan **YYYY-MM-DD_USERNAME** dan memindahkan gambar yang telah di ekstrak tersebut kedalam folder yang baru dibuat. urutan dari gambar diambil dari gambar paling akhir berdasarkan alphabetical order, diekstrak angkanya, lalu disesuaikan untuk dimasukkan ke loop. setelah semua gambar terdownload dan tertambahkan di folder baru, folder akan di zip lagi dengan menambahkan password sesuai dengan password user. lalu langkah terakhir adalah menghapus folder yang di unzip serta folder baru yang dibuat tadi supaya lebih rapih.

 kondisional kedua membuat folder baru yang disesuaikan dengan **YYYY-MM-DD_USERNAME** lalu mendownload gambar sebanyak num yang telah diinputkan user sebelumnya. gambar tersebut disimpan di folder yang baru kita buat lalu folder akan di zip dengan password yang sama dengan password login user. terakhir hapus folder baru yang dibuat supaya lebih rapih.
<br /> <br />
 ```
 else
		echo LOGIN: ERROR Failed login attempt on user $user
		echo "${t_d} ${t_h} LOGIN: ERROR Failed login attempt on user $user" >> $my_dir/log.txt
	fi
 ```
 kondisional ini tereksekusi ketika user salah memasukkan password(tidak cocok dengan yang ada pada user.txt). program akan mengoutputkan error lalu mengirimkannya kedalam log.txt
<br /> <br />
 ```
 else
	echo LOGIN: ERROR User not found
	echo "${t_d} ${t_h} LOGIN: ERROR User not found" >> $my_dir/log.txt
fi
 ```
kondisional ini tereksekusi ketika user memasukkan username yang tidak ada pada user.txt. program akan mengoutpukan error lalu mengirimkannya kedalam log.txt
<br /> <br /> <br /> <br />

### register.sh
```
my_dir="$(dirname "$0")"
t_d=$(date +"%m/%d/%Y")
t_h=$(date +"%T")
```
sebagai inisialisasi lokasi direktori beserta variabel yang menyimpan format tanggal dan waktu
<br /> <br />
```
if ! [ -d "$my_dir/users" ];then
	mkdir "$my_dir/users"
fi
```
membuat folder user jika folder user belum ada di direktori 
<br /> <br />
```
echo new username? 
read -s name
```
mengoutpukan pertanyaan pada user sebagai registrasi username baru. lalu username akan disimpan pada variabel name
<br /> <br />
```
if [ ${#name} -lt 8 ];then
	echo REGISTER: ERROR Username must be atleast 8 character 
	echo "${t_d} ${t_h} REGISTER: ERROR Username must be atleast 8 character" >> $my_dir/log.txt
elif ! [[ $name =~ [[:alnum:]] ]];then 
	echo REGISTER: ERROR Username must be alphanumerical
	echo "${t_d} ${t_h} REGISTER: ERROR Username must be alphanumerical" >> $my_dir/log.txt
elif ! [[ $name =~ [[:upper:]] ]];then
	echo REGISTER: ERROR Username must contain uppercase
	echo "${t_d} ${t_h} REGISTER: ERROR Username must contain uppercase" >> $my_dir/log.txt
elif ! [[ $name =~ [[:lower:]] ]];then
	echo REGISTER: ERROR Username must contain lowercase
	echo "${t_d} ${t_h} REGISTER: ERROR Username must contain lowercase" >> $my_dir/log.txt
elif grep -q "user: $name" "$my_dir/users/user.txt";then
	echo REGISTER: ERROR User already exists
	echo "${t_d} ${t_h} REGISTER: ERROR User already exists" >> $my_dir/log.txt
else 
		echo new password?
		read -s pass 
```
mengecek jika username yang diinputkan sesuai dengan kriteria. jika kurang dari 8 karakter maka akan mengoutputkan error lalu error akan dikirim ke log.txt. jika tidak memiliki alphanumeric(mengecek dengan menggunakan regex :alnum:) maka akan mengoutputkan error lalu error akan dikirim ke log.txt. jika tidak memiliki uppercase(mengecek dengan menggunakan regex :upper:) maka akan mengoutputkan error lalu error akan dikirim ke log.txt. jika tidak memiliki lowercase(mengecek dengan menggunakan regex :lower:) maka akan mengoutputkan error lalu error akan dikirim ke log.txt. jika username sudah ada di file user.txt maka akan mengoutputkan error lalu error akan dikirim ke log.txt

jika memenuhi semua syarat diatas maka akan lanjut ke else dimana program akan mengoutputkan pertanyaan pada user sebagai registrasi password dari username yang baru.
<br /> <br />
```
if [ "$name" == "$pass" ];then
		echo REGISTER: ERROR Password must be different than username
		echo "${t_d} ${t_h} REGISTER: ERROR Password must be different than username" >> $my_dir/log.txt
	elif [	${#pass} -lt 8 ];then
		echo REGISTER: ERROR Password must be atleast 8 character
		echo "${t_d} ${t_h} REGISTER: ERROR Password must be atleast 8 character" >> $my_dir/log.txt
	elif ! [[ $pass =~ [[:alnum:]] ]];then 
		echo REGISTER: ERROR Password must be alphanumerical
		echo "${t_d} ${t_h} REGISTER: ERROR Password must be alphanumerical" >> $my_dir/log.txt
	elif ! [[ $pass =~ [[:upper:]] ]];then
		echo REGISTER: ERROR Password must contain uppercase
		echo "${t_d} ${t_h} REGISTER: ERROR Password must contain uppercase" >> $my_dir/log.txt
	elif ! [[ $pass =~ [[:lower:]] ]];then
		echo REGISTER: ERROR Password must contain lowercase
		echo "${t_d} ${t_h} REGISTER: ERROR Password must contain lowercase" >> $my_dir/log.txt
	else
		echo REGISTER: INFO User ${name} registered successfully
		echo "${t_d} ${t_h} REGISTER: INFO User ${name} registered successfully" >> $my_dir/log.txt
		echo "user: ${name} pass: ${pass}" >> $my_dir/users/user.txt
	fi
```
mengecek jika password sesuai dengan kriteria. jika sama dengan username aka akan mengoutputkan error lalu error akan dikirim ke log.txt. jika kurang dari 8 karakter aka akan mengoutputkan error lalu error akan dikirim ke log.txt. jika tidak memiliki alphanumeric(mengecek dengan menggunakan regex :alnum:) maka akan mengoutputkan error lalu error akan dikirim ke log.txt. jika tidak memiliki uppercase(mengecek dengan menggunakan regex :upper:) maka akan mengoutputkan error lalu error akan dikirim ke log.txt. jika tidak memiliki lowercase(mengecek dengan menggunakan regex :lower:) maka akan mengoutputkan error lalu error akan dikirim ke log.txt.

jika memenuhi semua syarat diatas maka akan lanjut ke else dimana program akan mengoutputkan **REGISTER: INFO User ${name} registered successfully** lalu output akan dikirim ke log.txt. 

setelah itu program menyimpan username dan password yang telah memenuhi syarat tersebut menjadi satu line ke dalam users/user.txt
<p>SOAL 2
poin A membuatfolder dengan mkdir forensic_log_website_daffainfo_log

poin B
Menghitung rata rata penyerang website daffa.info, disini saya menggunakan awk untuk menyelesaikan problemnya, pertama saya menggunakan fungsi {split($0,a,":");b[a[2]":"a[3]"\""]++;} untuk memisahkan index dari kolom berdasarkan tanda :. lalu saya menghitung rata rata request dengan fungsi for(i in b){
	 counter++;
	 jumlah +=b[i];}
	 jumlah = jumlah/counter;

poin C
mencari ip yang paling banyak melakukan request dengan menggunakan fungsi awk '{
split($0,a,":");
b[a[1]]++;}END{
	ip;
	NilaiMax=0;
	for(i in b){
		if(NilaiMax < b[i]){
			ip = i;
			NilaiMax = b[ip]
		}
	}

poin D
mencari agent yang menggunakan curl
awk '/curl/ { ++n }
END{print "ada " n " request yang menggunakan curl sebagai user-agent"}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

poin E
menampilkan ip yang mengakses pada jam 2 dengan menggunakan awk '/22\/Jan\/2022:02/{
split($0,a,":")
b[a[1]]++;
counter++}END{
	for(i in b){
		print i "IP Address Jam 2 Pagi"	
	}
