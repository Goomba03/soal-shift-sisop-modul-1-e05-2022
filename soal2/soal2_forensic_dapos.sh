#!/bin/bash/

#Poin A Membuat folder dengan mkdir forensic_log_website_daffainfo_log

#Poin B Mencari rata-rata request per jam
awk '{split($0,a,":");
b[a[2]":"a[3]"\""]++;
}END{
	for(i in b){
	 counter++;
	 jumlah +=b[i];}
	 jumlah = jumlah/counter;
	printf "Rata-rata serangan adalah sebanyak " jumlah " request perjam"
}' log_website_daffainfo.log > forensic_log_website_daffainfo_log/ratarata.txt 

#Poin C Mencari IP yang paling sering mengakses website
awk '{
split($0,a,":");
b[a[1]]++;}END{
	ip;
	NilaiMax=0;
	for(i in b){
		if(NilaiMax < b[i]){
			ip = i;
			NilaiMax = b[ip]
		}
	}
print "IP yang paling banyak mengakses server adalah :" ip " sebanyak " NilaiMax" request"
}' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt

#Poin D menghitung banyaknya user agent yang menggunakan curl
awk '/curl/ { ++n }
END{print "ada " n " request yang menggunakan curl sebagai user-agent"}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

#Poin E menampilkan IP Address yang mengakses website pada tanggal 22 Januari 2022 pukul 2 pagi
awk '/22\/Jan\/2022:02/{
split($0,a,":")
b[a[1]]++;
counter++}END{
	for(i in b){
		print i "IP Address Jam 2 Pagi"	
	}
}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
