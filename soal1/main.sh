#!/bin/bash
my_dir="$(dirname "$0")"
t_d=$(date +"%m/%d/%Y")
t_h=$(date +"%T")
t_d1=$(date +"%Y-%m-%d")
echo enter your username
read -s name

if grep -q "user: $name" "$my_dir/users/user.txt";then
	echo enter your password
	read -s pass
	
	if grep -q "user: $name pass: $pass" "$my_dir/users/user.txt";then
		echo LOGIN: INFO User $user logged in
		echo "${t_d} ${t_h} LOGIN: INFO User $user logged in" >> $my_dir/log.txt
		echo 'enter command ("dl N(number)" or "att")'
		read cmd
		if [ "$cmd" == "att" ];then
			grep -c "LOGIN: " "$my_dir/log.txt"
		else 
			num=$(echo "$cmd" | sed 's/[^0-9]*//g')
			if [ -e $my_dir/"$t_d1"_$name.zip ];then
				unzip -P $pass $my_dir/"$t_d1"_$name.zip -d $my_dir/
				mkdir $my_dir/"$t_d1"_$name
				find $my_dir/$my_dir/"$t_d1"_$name -type f -print0 | xargs -0 mv -t $my_dir/"$t_d1"_$name
				last_file=$(ls -1 $my_dir/"$t_d1"_$name | tail -n 1)
				last_num=$(echo "$last_file" | sed 's/[^0-9]*//g')
				for((i=last_num+1; i<num+last_num+1; i++))
				do
				if [ $i -lt 10 ];then
					urut=0$i
				else
					urut=$i
				fi
				wget -nd -r -P $my_dir/"$t_d1"_$name / --output-document=$my_dir/"$t_d1"_$name/PIC_$urut.jpg https://loremflickr.com/320/240
				done
				zip --password $pass -r $my_dir/"$t_d1"_$name.zip $my_dir/"$t_d1"_$name
				base_dir=$(echo "$my_dir" | cut -d "/" -f2)
				rm -r $my_dir/$base_dir/
				rm -r $my_dir/"$t_d1"_$name/
			else
				if ! [ -d $my_dir/"$t_d1"_$name ];then
					mkdir $my_dir/"$t_d1"_$name
				fi
				for((i=1; i<num+1; i++))
				do
				if [ $i -lt 10 ];then
					urut=0$i
				else
					urut=$i
				fi
				wget -nd -r -P $my_dir/"$t_d1"_$name / --output-document=$my_dir/"$t_d1"_$name/PIC_$urut.jpg https://loremflickr.com/320/240
				done
				zip --password $pass -r $my_dir/"$t_d1"_$name.zip $my_dir/"$t_d1"_$name 
				rm -r $my_dir/"$t_d1"_$name/
			fi
		fi
	else
		echo LOGIN: ERROR Failed login attempt on user $user
		echo "${t_d} ${t_h} LOGIN: ERROR Failed login attempt on user $user" >> $my_dir/log.txt
	fi
else
	echo LOGIN: ERROR User not found
	echo "${t_d} ${t_h} LOGIN: ERROR User not found" >> $my_dir/log.txt
fi
